# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import codecs
import os
import sys

from bs4 import BeautifulSoup
import re
import hashlib
import shutil
import codecs
import requests
#os.path.join - compunere cale
SIZE_BLK=1024


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


def frecventa():
    l1 = [1, 1, 1, 30, 24 ,58, 66, 58]
    d = dict()
    i = 0
    """for x in l1:
        d[x] = 0
    for x in l1:
        d[x] += 1"""
    for x in l1:
        if x not in d:
            d[x] = 0
        d[x] += 1
    for k, v in d.items():
        print("cheia: {} valoarea: {}".format(k, v))


def subfoldere1(path1):
    for rootDirPath, subDirs, files in os.walk(path1):
        for file in files:
            print(os.path.join(rootDirPath, file))
        for dir in subDirs:
            print(os.path.join(rootDirPath, dir))


def subfoldere2(path1):
    for x in os.listdir(path1):
        if x != None:
            newPath=os.path.join(path1, x)
            print(newPath)
            if os.path.isdir(newPath):
                subfoldere2(newPath)



def paginaWeb(path1):
    dict1 = {}
    cnt = 0

    ok=1
    lista1 = []
    numeProprii=[]
    separators=["!", ".", "?"]
    fin = open(path1, encoding="utf8")
    soup = BeautifulSoup(fin, 'lxml')
    #text = soup.get_text()
    #print(text)
    text = "Ceva de scris, Cristian.?! Ana are mere?"
    lista1=re.findall("[a-zA-Z]*[aeiou][a-zA-Z]*[?!\.]*", text)

    #ma uit care sunt numele proprii
    for wrd in lista1:
        #print (wrd)
        if ok == 0 and re.search("[A-Z]", wrd[0]) and len(wrd)>=4:
            numeProprii.append(wrd)
        if any(c in wrd for c in separators) != False and (wrd.find(".") == len(wrd)-1 or wrd.find("!") == len(wrd)-1 or wrd.find("?") == len(wrd)-1):
            ok=1
        else:
            ok=0

    #modific lista care are la sf cuv separatorii
    words=[re.sub("[?!\.]+", '', word) for word in lista1] #inlocuieste caracterele in word(ala din paranteza)

    """for i in range(len(lista1)):
        if any(c in lista1[i] for c in separators) != False and (lista1[i].find(".") == len(lista1[i])-1 or lista1[i].find("!") == len(lista1[i])-1 or lista1[i].find("?") == len(lista1[i])-1):
            lista1[i] = lista1[i][:-1]
        if i<len(numeProprii) and any(c in numeProprii[i] for c in separators) != False and (numeProprii[i].find(".") == len(numeProprii[i]) - 1 or numeProprii[i].find("!") == len(numeProprii[i]) - 1 or numeProprii[i].find("?") == len(numeProprii[i]) - 1):
            numeProprii[i] = numeProprii[i][:-1]
            print ("Este nume propriu: ", numeProprii[i])"""

    #verific sa fie cuvinte
    for wrd in lista1:
        if len(wrd)>=4:
            if (wrd.lower() in dict1):
                dict1[wrd.lower()]+=1
            else:
                dict1[wrd.lower()]=1

    print("Total cuvinte: ", sum(dict1.values()))
    cnt=sum(dict1.values())
    print("Top 5 cele mai frecvente cuvinte: ")
    j = 0
    for wrd in sorted(dict1, key=dict1.get, reverse=True):
        print(wrd, ": " ,dict1[wrd])
        j += 1
        if j == 5:
            break
    
    fin.close()


def rename(fileName):
    splitName=fileName.split(".")
    newName=""
    for i in range(len(splitName)-1):
        newName=newName+splitName[i]
    return newName+".new"


def hashCalc(path1, fout):
    for x in os.listdir(path1):
        if x != None:
            if os.path.isdir(path1 + '/' + x):
                hashCalc(path1 + '/' + x, fout)
            if os.path.isfile(path1 + '/' + x) and x!="fout.txt" and x!="fout2.txt":
                file = path1 + '/' + x
                h = hashlib.sha256()
                fin=open(file, "rb")
                fb = 0
                while fb!=b'':
                    fb = fin.read(SIZE_BLK)
                    h.update(fb)
                fout.write(h.hexdigest())
                fout.write('\n')
                print (h.hexdigest())
                fin.close()


def hashVer(path1, fout):
    for x in os.listdir(path1):
        if x != None:
            if os.path.isdir(path1 + '/' + x):
                hashVer(path1 + '/' + x, fout)
            if os.path.isfile(path1 + '/' + x) and x != "fout.txt" and x != "fout2.txt":
                file = path1 + '/' + x
                line=fout.readline()
                if not line:
                    break
                h = hashlib.sha256()
                fin = open(file, "rb")
                fb = 0
                while fb != b'':
                    fb = fin.read(SIZE_BLK)
                    h.update(fb)
                if h.hexdigest()+'\n' != line:
                    print("Fisierul " + x + " este defect")
                    shutil.copy(path1 + '/' + x, "C:/Users/Mihai/infected"+ '/' + x)
                    os.rename("C:/Users/Mihai/infected"+ '/' + x, "C:/Users/Mihai/infected"+ '/' + rename(x))
                    print(h.hexdigest())
                    fin.close()



# Press the green button in the gutter to run the script.
#puteam sa fac o fct main pe care sa o apelez def main
if __name__ == '__main__':
    """command = "search"
    if sys.argv[1] == "prepare":
        with open("C:/Users/Mihai/FolderCuFisire/fout.txt", "w") as fout:
            hashCalc("C:/Users/Mihai/FolderCuFisire", fout)
    else:
        if sys.argv[1] == "search":
            fout=open("C:/Users/Mihai/FolderCuFisire/fout.txt", "r")
            hashVer("C:/Users/Mihai/FolderCuFisire", fout)
        else:
            print("ceva")"""

    frecventa()
    #subfoldere2(r'C:\Users\Mihai\Desktop\Examenegc')
    #subfoldere1(r'C:\Users\Mihai\Desktop\Examenegc')
    #paginaWeb("C:/Users/Mihai/Documents/Nikola Tesla - Wikipedia.html")
    #print(rename("ceva.txt"))
    #fout.close()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
